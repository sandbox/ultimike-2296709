/**
 * @file
 * Some basic behaviors and utility functions for drupaleasy_d8_theme_training.
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.ded8tt = {};

  Drupal.behaviors.ded8ttFitText = {
    attach: function (context, settings) {
      $('.front .name-and-slogan', context).once('fitText', function () {
        $(this).fitText();
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
